/*!
\file    serialib.h
\brief   Header file of the class serialib. This class is used for communication over a serial device.
\author  Philippe Lucidarme (University of Angers)
\version 2.0
\date    december the 27th of 2019
This Serial library is used to communicate through serial port.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE X CONSORTIUM BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
This is a licence-free software, it can be used by anyone who try to build a better world.
*/
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/videoio/videoio_c.h>
#include "opencv2/core/core.hpp" 
#include <opencv2/imgcodecs.hpp>

using namespace cv;
#include <string>
#define COPPELIA 1
using namespace std;
#define BSIZE 100

class serialib
{
public:

    //_____________________________________
    // ::: Constructors and destructors :::



    // Constructor of the class
    serialib    ();

    // Destructor
    ~serialib   ();



    //_________________________________________
    // ::: Configuration and initialization :::


    // Open a device
    char    openDevice (const char *Device,const unsigned int Bauds);

    // Close the current device
    void    closeDevice();

	char writeString(const char *txt);

};


string readline(serialib *serial);
void clear_buffer(serialib *serial);
void sim_init(int clientID);
void initCamara();
Mat getImage();