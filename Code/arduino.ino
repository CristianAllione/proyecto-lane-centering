//--------------------------------------------------------------------------------------------------------------------
//VARIABLES PARA EL CONTROL DE DIRECCION
#include <Servo.h>

//Timeout
int timeout = 0;
int count = 0;

//variables que definen cuánto tiempo se espera la comunicación con Raspy, pasados "demora permitida" [ms], arduino asume conexion fallida
int dt = 10; //saltos unitarios
int demora_permitida = 500; //[ms] que espera arduino el msj antes de asumir el fallo

int delta=0; //angulo de las ruedas delanteras
String inputString = "";
Servo myservo;  // create servo object to control a servo
int retraso= 9;
int grados=0;
//----------------------------------------------------------------------------------------------------------------------
//VARIABLES PARA EL CONTROL DE VELOCIDAD
/// Variables //////////////////////////////////////////////////////////////////////////////////////////////////////////////
int pwm_control=0;

int encoder_pin1 = 2;             //Pin 2- ENCODER IZQUIERDO     
unsigned int delta_time1 [3] = {0,0,0};                  //Velocidad en [Km/h]
unsigned int timeold1 = 0;  // Tiempo 

int encoder_pin2 = 3;             // pIN 3 - ENCODER DERECHO
unsigned int delta_time2  [3] = {0,0,0};                  //Velocidad en [Km/h]
unsigned int timeold2 = 0;  // Tiempo 
//NOTA: se recomienda el uso de vairblaes volatile dentro de atachinterrupt  - 
//Observación:   attachInterrupt(digitalPinToInterrupt(pin), ISR, mode) (recommended)


unsigned int delta_time=0; //valor enviado a raspy: consiste en la suma de 6 valores de lapso de tiempo entre dos pulsos

//MOTOR IZQUIERDO
const int pinENA = 4; //NO PWM
const int pinIN1 = 5; //PWM
const int pinIN2 = 6; //PWM
//MOTOR DERECHO
const int pinIN3 = 10;
const int pinIN4 = 11;
const int pinENB = 12;

//const int waitTime = 2000;   //espera entre fases

const int pinMotorA[3] = { 
  pinENA, pinIN1, pinIN2 };
const int pinMotorB[3] = { 
  pinENB, pinIN3, pinIN4 };


//------------------------------------------------------------------------------------------------------------------------
//TRAMA PRINCIPAL
void setup() {
  setup_serial();

  myservo.attach(9);  // attaches the servo on pin 9 to the servo object

  pinMode(encoder_pin1, INPUT); // Configuración del pin nº2
  pinMode(encoder_pin2, INPUT); // Configuración del pin nº3
  
  attachInterrupt(0, counter1, RISING); // Configuración de la interrupción para encoder IZQUIERDO 
  attachInterrupt(1, counter2, RISING); // Configuración de la interrupción para encoder DERECHO  
  

  pinMode(pinIN1, OUTPUT); //Motores
  pinMode(pinIN2, OUTPUT);
  pinMode(pinENA, OUTPUT);
  pinMode(pinIN3, OUTPUT);
  pinMode(pinIN4, OUTPUT);
  pinMode(pinENB, OUTPUT);
}


void loop() {
  // put your main code here, to run repeatedly:
  //Serial.println("flag");
    
  send_message();
  
  process_serial();

  
}

//--------------------------------------------------------------------------------------------------------------------------------
//FUNCIONES 

void(* resetFunc) (void) = 0; //declare reset function @ address 0

void setup_serial()
{
  // initialize serial:
  Serial.begin(115200);
  Serial.println("A: Long message to mark beggining");
  // reserve 200 bytes for the inputString:
  inputString.reserve(200);


}

void send_message() {
  delta_time=0;
  delta_time+=delta_time1[0];
  delta_time+=delta_time1[1];
  delta_time+=delta_time1[2];
  
  delta_time+=delta_time2[0];
  delta_time+=delta_time2[1];
  delta_time+=delta_time2[2];
  
  String txt=String((int)delta_time);
  Serial.println(txt);
}

void process_serial()
{
  //Serial.println("process_serial de arduino");
  inputString = "";
  while (1) {
    if (Serial.available()) {
      timeout = 0;
      // get the new byte:
      char inChar = (char)Serial.read();

      // add it to the inputString:
      if (inChar == '\n') //Esto significa que se llego al final del mensaje
      {
        break;
      }

      inputString += inChar;
    }
    else {
      timeout += 1;
      if (timeout > demora_permitida/dt)
      {
        timeout = 0;
        myservo.write(90+retraso); //90grados equivalen al auto andando derecho - grados<90 gira haciua izq - grados>90 gira hacia derecha
        moveForward(pinMotorA, 0);
        moveForward(pinMotorB, 0);
        //Se nos desconecto la raspi?
        //Esto representaria que despues de 2 segundos no se recibio nada del codigo C++
        //Aca iria el codigo para frenar los motores si se perdio la comunicacion
      }
      delay(dt);  
    }
  }

  //Procesamos el mensaje

  if (inputString=="reset"){
    resetFunc();
  }


  String txt1="";
  String txt2="";
  int aux=inputString.indexOf("/"); //marca de division del mensaje de entrada

  //if(inputString.startsWith("delta=")){

  txt1=txt1+inputString.substring(0,aux);  
  //Serial.println("A: entro comando de angulo: "+txt1); //Serial.println();
  grados=(txt1).toInt();

  myservo.write(90+retraso-grados); ///grados
  //Serial.println(txt+" grados - prende luz");
  //digitalWrite(LED_BUILTIN,HIGH);

  txt2=txt2+inputString.substring(aux+1);   //arduino recibe el valor entero PWM de accion de control, que se castea a +-6V en el puente H
  //Serial.print("A: entro comando de velocidad: "+txt2); //Serial.println();
  pwm_control=(txt2).toInt(); //casteo a entero, innecesario.

  moveForward(pinMotorA, pwm_control);
  moveForward(pinMotorB, pwm_control);

}




void counter1(){
  if(  digitalRead (encoder_pin1) ){ //&& ( millis() - timeold1 ) > 0.5  
    
     unsigned int aux=millis()-timeold1;
     
     timeold1=millis();
     
     delta_time1[0]=delta_time1[1];
     delta_time1[1]=delta_time1[2];
     delta_time1[2]=aux;
     
  } 
  
} 


void counter2(){
  if(  digitalRead (encoder_pin2)  ){ //&& ( millis() - timeold1 ) > 0.5
    
     unsigned int aux=millis()-timeold2;
     
     timeold2=millis();
     
     delta_time2[0]=delta_time2[1];
     delta_time2[1]=delta_time2[2];
     delta_time2[2]=aux;
     
  } 
} 




void moveForward(const int pinMotor[3], int vel)
{
  analogWrite(pinMotor[1], 0);
  analogWrite(pinMotor[2], vel);

  digitalWrite(pinMotor[0], HIGH);
}

void moveBackward(const int pinMotor[3], int vel)
{
  analogWrite(pinMotor[1], vel);
  analogWrite(pinMotor[2], 0);

  digitalWrite(pinMotor[0], HIGH);
}

void fullStop(const int pinMotor[3])
{
  analogWrite(pinMotor[1], 0);
  analogWrite(pinMotor[2], 0);

  digitalWrite(pinMotor[0], HIGH);
}

